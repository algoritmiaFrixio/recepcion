// Quantity buttons
function qtySum() {
    const arr = document.getElementsByName('qtyInput');
    let tot = 0;
    for (let i = 0; i < arr.length; i++) {
        if (parseInt(arr[i].value))
            tot += parseInt(arr[i].value);
    }
    const cardQty = document.querySelector('.qtyTotal');
    cardQty.innerHTML = tot.toString();
}

qtySum();
$('.qtyButtons input').after('<div class="qtyInc"></div>');
$('.qtyButtons input').before('<div class="qtyDec"></div>');
$('.qtyDec, .qtyInc').on('click', function () {
    const $button = $(this);
    const oldValue = $button.parent().find('input').val();
    let newVal;
    if ($button.hasClass('qtyInc')) {
        let newVal = parseFloat(oldValue) + 1;
        $button.parent().find('input').val(newVal);
    } else {
        // don't allow decrementing below zero
        if (oldValue > 0) {
            let newVal = parseFloat(oldValue) - 1;
            $button.parent().find('input').val(newVal);
        } else {
            newVal = 0;
            $button.parent().find('input').val(newVal);
        }
    }
    qtySum();
    $('.qtyTotal').addClass('rotate-x');
});

function removeAnimation() { $('.qtyTotal').removeClass('rotate-x'); }

const counter = document.querySelector('.qtyTotal');
counter.addEventListener('animationend', removeAnimation);
