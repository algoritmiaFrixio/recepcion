<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use LVR\CreditCard\CardCvc;
use LVR\CreditCard\CardExpirationMonth;
use LVR\CreditCard\CardExpirationYear;
use LVR\CreditCard\CardNumber;

class StoreCartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'last_name' => 'nullable|string',
            'country' => 'required|string',
            'state' => 'nullable|string',
            'city' => 'nullable|string',
            'company_name' => 'nullable|string',
            'address' => 'required|string',
            'email' => 'required|email',
            'code' => 'nullable|string',
            'phone' => 'required|string',
            'comments' => 'nullable|string',
            'name_card' => 'required|string',
            'number_card' => ['required', new CardNumber],
            'expiration_date_year' => ['required', new CardExpirationYear($this->get('expiration_date_year'))],
            'expiration_date_month' => ['required', new CardExpirationMonth($this->get('expiration_date_month'))],
            'security_code' => ['required', new CardCvc($this->get('security_code'))],
        ];
    }
}
