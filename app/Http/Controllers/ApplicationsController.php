<?php

namespace App\Http\Controllers;

use App\Applications;
use Illuminate\Http\Request;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class ApplicationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $application = Applications::where('cargo_name', $request->habilidades)->first();
        return view('datos-hoja', compact('application'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $application = Applications::where('cargo_name', $request->habilidades)->first();
        $application->enterprise;
        return view('datos-hoja', compact('application'));
    }

    public function getApplications()
    {
        return response()->json(Applications::all(), 200);
    }

    private function mailer(string $subject, string $email, string $path)
    {
        $transport = (new Swift_SmtpTransport('neurocrece.co', 465, 'ssl'))
            ->setUsername('info@neurocrece.co')
            ->setPassword('*ppc5cqpPf;[');
        $mailer = new Swift_Mailer($transport);
        $message = (new Swift_Message())
            ->setSubject($subject)
            ->setFrom(['info@neurocrece.co' => 'Solocitud Hoja de vida'])
            ->setTo($email)
            ->attach(\Swift_Attachment::fromPath($path));
        return $mailer->send($message);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Applications $applications
     * @return \Illuminate\Http\Response
     */
    public function show(Applications $applications)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Applications $applications
     * @return \Illuminate\Http\Response
     */
    public function edit(Applications $applications)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Applications $applications
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Applications $applications)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Applications $applications
     * @return \Illuminate\Http\Response
     */
    public function destroy(Applications $applications)
    {
        //
    }
}

