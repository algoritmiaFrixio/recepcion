<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class HotelsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

//        dd($contentResponse[0]);
        return view('pages.hoteles.view-hoteles');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $requestController)
    {
        $city = $requestController->get('city');
        $dates = explode('>', $requestController->get('dates'));
        $startDate = $dates[0];
        $endDate = $dates[1];
        $qtyAdult = $requestController->get('qtyInput');
        $qtyChildren = $requestController->get('qtyInputC');
        $client = new \GuzzleHttp\Client();
        $request = $client->get('http://134.209.123.87/api/availHotel/' . $startDate . '/' . $endDate . '/' .
            $qtyAdult . '/' . $qtyChildren);
        $hotels = json_decode($request->getBody());
        foreach ($hotels as $key => $value) {
                dd($hotels[$key]);
        }
//        array_push($contentResponse, json_decode($response));
//        dd($hotels);
//        return response()->json($hotels);
//        return view('pages.hoteles.view-hoteles', compact('hotels'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
