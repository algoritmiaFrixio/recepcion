<?php

namespace App\Http\Controllers;

use App\Vacante;
use Illuminate\Http\Request;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class VacanteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tmp = time() . $request->file('hv')->getClientOriginalName();
        $request->file('hv')->move('../files/', $tmp);
        $arrNames = 'http://vacantes.neurocrece.co/files/' . $tmp;
        $vacante = new Vacante();
        $vacante->fill($request->all());
        $vacante->path = $arrNames;
        if ($vacante->save()) {
            if ($this->mailer('Recepcion hoja de vida', 'sebascano65@gmail.com', $arrNames)) {
                $success = true;
                return view('success', compact('success'));
            }
        }
        return view('success');
    }

    private function mailer(string $subject, string $email, string $path)
    {
        $transport = (new Swift_SmtpTransport('neurocrece.co', 465, 'ssl'))
            ->setUsername('info@neurocrece.co')
            ->setPassword('*ppc5cqpPf;[');
        $mailer = new Swift_Mailer($transport);
        $message = (new Swift_Message())
            ->setSubject($subject)
            ->setFrom(['info@neurocrece.co' => 'Hoja de vida'])
            ->setTo($email)
            ->attach(\Swift_Attachment::fromPath($path));
        return $mailer->send($message);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Vacante $vacante
     * @return \Illuminate\Http\Response
     */
    public function show(Vacante $vacante)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Vacante $vacante
     * @return \Illuminate\Http\Response
     */
    public function edit(Vacante $vacante)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Vacante $vacante
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vacante $vacante)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Vacante $vacante
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vacante $vacante)
    {
        //
    }
}
