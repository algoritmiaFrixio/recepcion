<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applications extends Model
{
    protected $hidden = ['updated_at'];

    protected $fillable = [
        "boss_immediate",
        "spot",
        "id_enterprise",
        "id_gender",
        "cargo_name",
        "age_range",
        "placement",
        "commission",
        "salary_assignment",
        "requested_profile",
        "features",
        "requirements",
        "basic_knowledge",
        "sheet_reception",
        "chief_phone",
        "investment_networks",
        "time_diffusion"
    ];

    protected function enterprise()
    {
        return $this->belongsTo(Enterprise::class, 'id_enterprise', 'id');
    }

    protected function gender()
    {
        return $this->belongsTo(Gender::class, 'id_gender', 'id');
    }
}
