<?php

namespace App\Imports;

use App\Tickets;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class TicketsDisneyImport implements ToModel, WithCustomCsvSettings, WithChunkReading
{
    use Importable;

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        dump($row);
        return new Tickets([
            'name' => $row[0],
            'price' => $row[1],
            'type' => $row[2],
            'date' => $row[3],
            'description' => $row[4],
            'days' => $row[5],
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';'
        ];
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
