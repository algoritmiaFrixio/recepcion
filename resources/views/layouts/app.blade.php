<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
          content="Encuentra lugares fantásticos viajando por el mundo en cruceros, visitando los parques temáticos
          de Orlando Florida y compartiendo con familia y amigos.">
    <meta name="author" content="RedCode">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Ticketland') }} @yield('title')</title>
    <link rel="icon" type="image/x-icon" href="img/favicon.ico">

{{--    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />--}}
<!-- Scripts -->
{{--    <script src="{{ asset('js/app.js') }}" defer></script>--}}
<!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800" rel="stylesheet">


    <!-- BASE CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/vendors.css" rel="stylesheet">
    <!-- YOUR CUSTOM CSS -->
    <link href="css/custom.css" rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('datepicker/datepicker.css') }}" rel="stylesheet">
</head>
<body>
<div id="page">

    <!-- /header -->
    @yield('content')
    <footer>
        <div class="container margin_60_35">
            <div class="row">
                <div class="col-lg-5 col-md-12 p-r-5">
                    <p><img src="img/logo.png" width="150" height="125" data-retina="true" alt=""></p>
                    <div class="follow_us">
                        <ul>
                            <li>Síguenos</li>
                            <li><a href="https://www.facebook.com/NeuroCrece/" target="__blank"><i
                                        class="ti-facebook"></i></a></li>
                            <li><a href="https://www.instagram.com/neurocrece/" target="__blank"><i
                                        class="ti-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 ml-lg-auto">
                </div>
                <div class="col-lg-3 col-md-6">
                    <h5>Contáctanos</h5>
                    <ul class="contacts">
                        <li><a href="tel:+573144389974"><i class="ti-mobile"></i> +57 314 4389974</a></li>
                        <li><a href="mailto:neurocrece@gmail.com"><i class="ti-email"></i>
                                neurocrece@gmail.com</a></li>
                    </ul>
                </div>
            </div>
            <!--/row-->
            <hr>
            <div class="row">
                <div class="col-lg-6">
                    <ul id="footer-selector">
                        <li><img src="img/cards_all.svg" alt=""></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!--/footer-->
</div>
<!-- page -->

<!-- Sign In Popup -->
<!-- /Sign In Popup -->
<div id="toTop"></div><!-- Back to top button -->
<!-- COMMON SCRIPTS -->
<script src="js/jquery-2.2.4.min.js"></script>
<script src="js/common_scripts.js"></script>
<script src="js/main.js"></script>
<script src="assets/validate.js"></script>

<!-- Map -->
<script src="js/markerclusterer.js"></script>

<!-- Masonry Filtering -->
<script src="js/isotope.min.js"></script>
<script>
    document.addEventListener('load', () => {
        var $container = $('.isotope-wrapper');
        $container.isotope({itemSelector: '.isotope-item', layoutMode: 'masonry'});

        $('.filters_listing').on('click', 'input', 'change', function () {
            var selector = $(this).attr('data-filter');
            $('.isotope-wrapper').isotope({filter: selector});
        });
    });
</script>

<!-- Range Slider -->
<script>
    document.addEventListener('load', () => {
        $('#range').ionRangeSlider({
            hide_min_max: true,
            keyboard: true,
            min: 30,
            max: 180,
            from: 60,
            to: 130,
            type: 'double',
            step: 1,
            prefix: 'Km ',
            grid: false
        });
    });
</script>
<script src="{{asset('datepicker/datepicker.js')}}"></script>
<script src="{{asset('datepicker/datepicker.es-ES.js')}}"></script>
<script src="js/input_qty.js"></script>

</body>
</html>
