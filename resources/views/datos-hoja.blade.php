@extends('layouts.app')

@section('content')
    <main>
        <div class="hero_in cart_section">
            <div class="wrapper">
                <div class="container">
                    <div class="bs-wizard clearfix">
                        <div class="bs-wizard-step">
                            <div class="text-center bs-wizard-stepnum">Selecionar Vacante</div>
                            <div class="progress">
                                <div class="progress-bar"></div>
                            </div>
                            <a href="/" class="bs-wizard-dot"></a>
                        </div>
                        <div class="bs-wizard-step active">
                            <div class="text-center bs-wizard-stepnum">Llenar solicitud</div>
                            <div class="progress">
                                <div class="progress-bar"></div>
                            </div>
                            <a href="" class="bs-wizard-dot"></a>
                        </div>

                        <div class="bs-wizard-step disabled">
                            <div class="text-center bs-wizard-stepnum">Final!</div>
                            <div class="progress">
                                <div class="progress-bar"></div>
                            </div>
                            <a href="" class="bs-wizard-dot"></a>
                        </div>
                    </div>
                    <!-- End bs-wizard -->
                </div>
            </div>
        </div>
        <!--/hero_in-->
        <div class="bg_color_1">
            <div class="container margin_60_35">
                <form class="row" Method="POST" action="{{route('vacante.store')}}" enctype="multipart/form-data"
                      id="form-buy">
                    @csrf
                    <div class="col-lg-8">
                        <div class="box_cart">
                            <div class="form_title">
                                <h3><strong>1</strong>Información general</h3>
                            </div>
                            <div class="step">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="nombre">Nombre completo*</label>
                                            <input type="text" class="form-control"
                                                   name="nombre" id="nombre" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="correo">Correo electrónico*</label>
                                            <input type="email" id="correo"
                                                   class="form-control" name="correo" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="telefono">Teléfono*</label>
                                            <input type="number" id="telefono"
                                                   class="form-control" name="telefono" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <!--End step -->
                            <div class="form_title">
                                <h3><strong>2</strong>Hoja de vida</h3>
                            </div>
                            <div class="step">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="hv">Subir hoja de vida*</label>
                                        <input type="file"
                                               class="form-control" name="hv" id="hv"
                                               accept="application/.pdf, .xls, .xlsx, .docx,.doc" required>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <!--End step -->
                            <div id="policy">
                                <p class="nomargin">
                                    Tus datos personales se utilizarán para procesar tu petición, mejorar tu experiencia
                                    en esta web y otros propósitos descritos en nuestra <a
                                        href="https://bordadosdecartago.com/wp-content/uploads/2016/12/Privacidad.pdf">
                                        política de privacidad.
                                    </a>
                            </div>
                        </div>
                    </div>
                    <!-- /col -->
                    <aside class="col-lg-4" id="sidebar">
                        <div class="box_detail">
                            <div id="total_cart">
                                Nombre vacante:
                                <br>
                                <span>{{$application->cargo_name}}</span>
                            </div>
                            <ul class="cart_details">
                                Empresa:
                                <span>{{$application->enterprise->name}}</span>
                            </ul>
                            <ul class="cart-details">
                                Requisitos:
                                <br>
                                <span>{{$application->requirements}}</span>
                            </ul>

                            <ul class="cart-details">
                                Perfil:
                                <br>
                                <span>{{$application->requested_profile}}</span>
                            </ul>
                            <button type="submit" class="btn_1 full-width purchase">Finalizar</button>
                            <div class="text-center">
                            </div>
                        </div>
                    </aside>
                </form>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /bg_color_1 -->
    </main>

@endsection
