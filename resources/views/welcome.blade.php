@extends('layouts.app')

@section('content')

    <main>
        <section class="hero_single version_2">
            <div class="wrapper">
                <div class="container">
                    <h3>VACANTES GRUPO EMPRESARIAL GEPCOL</h3>
                    <p>Registrate para tener acceso a las vacantes de nuestra compañía</p>
                    <form method="GET" action="{{route('application.store')}}">
                        @csrf
                        <div class="row no-gutters custom-search-input-2">
                            @include('partial.hero-form-hotel' )
                            <div class="col-lg-2">
                                <input type="submit" class="btn_search" value="Buscar">
                            </div>
                        </div>
                        <!-- /row -->
                    </form>
                </div>
            </div>
        </section>
        <!-- /hero_single -->

        <div class="container-fluid margin_30_95 pl-lg-5 pr-lg-5">
            <section class="add_bottom_45">
                <div class="row">
                    <div class="col-xl-12">
                        <h3>Nuestras empresas</h3>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-6">
                        <a href="https://www.facebook.com/PachoAnca/" target="_blank" class="grid_item">
                            <figure>
                                <img src="img/logo-pacho anca.png" class="img-fluid img-responsive-heigth"
                                     alt="Universal Studios Florida">
                                <div class="info">
                                    <h3>Pacho anca</h3>
                                </div>
                            </figure>
                        </a>
                    </div>
                    <!-- /grid_item -->
                    <div class="col-xl-3 col-lg-6 col-md-6">
                        <a href="https://www.facebook.com/GepcolClaro/" target="_blank" class="grid_item">
                            <figure>
                                <img src="img/logo gepcol-02.png" class="img-fluid img-responsive-heigth"
                                     alt="Universal’s Islands of Adventure ">
                                <div class="info">
                                    <h3>Gepcol claro</h3>
                                </div>
                            </figure>
                        </a>
                    </div>
                    <!-- /grid_item -->
                    <div class="col-xl-3 col-lg-6 col-md-6">
                        <a href="https://www.facebook.com/Moteroshop/" target="_blank" class="grid_item">
                            <figure>
                                <img src="img/Logo Moteros Shop.png" class="img-fluid img-responsive-heigth" alt="Universal’s
                                Volcano Bay">
                                <div class="info">
                                    <h3>Moteros shop</h3>
                                </div>
                            </figure>
                        </a>
                    </div>
                    <!-- /grid_item -->
                    <div class="col-xl-3 col-lg-6 col-md-6">
                        <a href="https://www.facebook.com/seguteccamarasdeseguridad/" target="_blank" class="grid_item">
                            <figure>
                                <img src="img/logo-3.png" class="img-fluid img-responsive-heigth"
                                     alt="Universal Orlando Resort">
                                <div class="info">
                                    <h3>Segutec</h3>
                                </div>
                            </figure>
                        </a>
                    </div>
                    <!-- /grid_item -->
                </div>
                <!-- /row -->
            </section>
            <section class="add_bottom_45">
                <div class="row">
                    <div class="col-xl-3 col-lg-6 col-md-6">
                        <a href="https://www.facebook.com/EvolutionBoutiqueStore/" target="_blank" class="grid_item">
                            <figure>
                                <img src="img/logo (2).png" class="img-fluid img-responsive-heigth"
                                     alt="Universal Studios Florida">
                                <div class="info">
                                    <h3>Boutique evolution</h3>
                                </div>
                            </figure>
                        </a>
                    </div>
                    <!-- /grid_item -->
                    <div class="col-xl-3 col-lg-6 col-md-6">
                        <a href="#" class="grid_item">
                            <figure>
                                <img src="img/logo-teo2.png" class="img-fluid img-responsive-heigth"
                                     alt="Universal’s Islands of Adventure ">
                                <div class="info">
                                    <h3>Teo</h3>
                                </div>
                            </figure>
                        </a>
                    </div>
                    <!-- /grid_item -->
                    <div class="col-xl-3 col-lg-6 col-md-6">
                        <a href="https://www.facebook.com/AlmacenesElPunto/" target="_blank" class="grid_item">
                            <figure>
                                <img src="img/LOGOS-EL-PUNTO.png" width="50" class="img-fluid img-responsive-heigth"
                                     alt="Universal’s
                                Volcano Bay">
                                <div class="info">
                                    <h3>Almacenes el Punto</h3>
                                </div>
                            </figure>
                        </a>
                    </div>
                    <!-- /grid_item -->
                    <div class="col-xl-3 col-lg-6 col-md-6">
                        <a href="https://www.facebook.com/bordadosdecartagoFrixio/" target="_blank" class="grid_item">
                            <figure>
                                <img src="img/logo1.png" class="img-fluid img-responsive-heigth"
                                     alt="Universal Orlando Resort">
                                <div class="info">
                                    <h3>Frixio</h3>
                                </div>
                            </figure>
                        </a>
                    </div>
                    <!-- /grid_item -->
                </div>
                <!-- /row -->
            </section>
            <section class="add_bottom_45">
                <div class="row">
                    <div class="col-xl-3 col-lg-6 col-md-6">
                        <a href="#" class="grid_item">
                            <figure>
                                <img src="img/Logo-Neuro-Crece.png" class="img-fluid"
                                     alt="Universal Studios Florida">
                                <div class="info">
                                    <h3>Neurocrece</h3>
                                </div>
                            </figure>
                        </a>
                    </div>
                    <!-- /grid_item -->
                </div>
                <!-- /row -->
            </section>
        </div>
    </main>
@endsection
