@extends('layouts.app')
@section('content')
    <main>
        <div class="hero_in cart_section last">
            <div class="wrapper">
                <div class="container">
                    <div class="bs-wizard clearfix">
                        <div class="bs-wizard-step">
                            <div class="text-center bs-wizard-stepnum">Selecionar Vacante</div>
                            <div class="progress">
                                <div class="progress-bar"></div>
                            </div>
                            <a href="" class="bs-wizard-dot"></a>
                        </div>

                        <div class="bs-wizard-step">
                            <div class="text-center bs-wizard-stepnum">Llenar solicitud</div>
                            <div class="progress">
                                <div class="progress-bar"></div>
                            </div>
                            <a href="" class="bs-wizard-dot"></a>
                        </div>

                        <div class="bs-wizard-step active">
                            <div class="text-center bs-wizard-stepnum">Final!</div>
                            <div class="progress">
                                <div class="progress-bar"></div>
                            </div>
                            <a href="#" class="bs-wizard-dot"></a>
                        </div>
                    </div>
                    <!-- End bs-wizard -->
                    @if(isset($success))
                        <div id="confirm">
                            <h4>Solicitud completado!</h4>
                            <p>Gracias por enviarnos tu hoja de vida nos comunicaremos contigo</p>
                        </div>
                    @else
                        <div id="confirm">
                            <h4>Ocurrió un error!</h4>
                            <p>Ocurrió un error en la operacion por favor informanos en <a
                                    href="mailto:info@neurocrece.co">info@neurocrece.co</a></p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <!--/hero_in-->
    </main>

    <!--/main-->
@endsection

