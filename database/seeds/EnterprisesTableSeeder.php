<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EnterprisesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('enterprises')->insert([
            'name' => "Frixio"
        ]);
        DB::table('enterprises')->insert([
            'name' => "Gepcol Claro"
        ]);
        DB::table('enterprises')->insert([
            'name' => "Almacenes el Punto"
        ]);
        DB::table('enterprises')->insert([
            'name' => "Almacen Punto hogar"
        ]);
        DB::table('enterprises')->insert([
            'name' => "Evolution"
        ]);
        DB::table('enterprises')->insert([
            'name' => "Hotel el bordado"
        ]);
        DB::table('enterprises')->insert([
            'name' => "Pacho anca"
        ]);
        DB::table('enterprises')->insert([
            'name' => "Estacion de servicio"
        ]);
    }
}
